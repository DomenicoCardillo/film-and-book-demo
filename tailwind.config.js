module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: {
    enabled: process.env.BUILD_ENV === 'production',
    mode: 'layers',
    content: [
      './src/**/*.js',
      './src/**/*.jsx',
      './src/**/*.html'
    ],
  },
  theme: {
    fontFamily: {
      sans: [
        'Open Sans',
        'sans-serif',
      ],
      fontWeight: {
        normal: '400',
        semibold: '600',
        bold: '700',
      },
    },
    extend: {
      colors: {
        black: '#333',
        primary: '#ba57e9',
        success: '#00be73',
        warning: '#ff9d1a',
        danger: '#ff3f32',
        gray: {
          100: '#f2f2f7',
          200: '#edf2f7',
          300: '#e2e8f0',
          400: '#cbd5e0',
          500: '#a0aec0',
          600: '#718096',
          700: '#4a5568',
          800: '#2d3748',
          900: '#1a202c',
        },
      },
      height: {
        '50vh': '50vh'
      }
    },
  },
  variants: {},
  plugins: [],
}
