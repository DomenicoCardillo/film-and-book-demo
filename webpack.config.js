const path = require('path')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = (_, argv) => {
  const isProduction = argv.mode === 'production'
  
  const config = {
    entry: './index',
    context: path.resolve(__dirname, 'src'),
    output: {
      publicPath: '/',
      path: path.resolve(__dirname, 'dist'),
      filename: 'bundle.[contenthash].js',
    },
    resolve: {
      extensions: ['.js', '.jsx']
    },
    devtool: isProduction
        ? 'nosources-source-map'
        : 'source-map',
    module: {
      rules: [
        {
          test: /\.(png|jpe?g|gif)$/i,
          use: [
            {
              loader: 'file-loader',
            },
          ],
        },
        {
          test: /\.css$/,
          use: [
            isProduction
                ? MiniCssExtractPlugin.loader
                : 'style-loader',
            'css-loader',
            'postcss-loader'
          ]
        },
        {
          test: /\.(js|jsx)/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader'
          }
        },
      ]
    },
    devServer: {
      historyApiFallback: true,
    },
    plugins: [
      new HtmlWebPackPlugin({
        template: 'index.html'
      }),
    ]
  }
  
  if (isProduction) {
    config.plugins = [
      ...config.plugins,
      new MiniCssExtractPlugin({
        filename: 'bundle.[contenthash].css',
      })
    ]
  }
  
  return config
}
