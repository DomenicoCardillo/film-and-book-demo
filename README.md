# Film and Book Demo

A demo of the project is available [here](https://film-and-book-demo.web.app/)

##### Local development
Copy your `src/config.sample.js` in `src/config.js` and edit with your API Keys

```bash 
$ cp src/config.sample.js src/config.js
```

Download modules and build the project

```bash
$ npm install
$ npm start
```