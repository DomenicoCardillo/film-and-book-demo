const reducer = (state, action) => {
  switch (action.type) {
  case 'SAVE_SEARCH_QUERY': {
    const { query } = action.payload
  
    return {
      ...state,
      query,
    }
  }
  case 'SAVE_SEARCH_DATA': {
    const { data } = action.payload
  
    return {
      ...state,
      data,
    }
  }
  default:
    return state
  }
}

export default reducer