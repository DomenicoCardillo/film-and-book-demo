export const saveSearchQuery = ({ dispatch, query }) => {
  return dispatch({
    type: 'SAVE_SEARCH_QUERY',
    payload: { query }
  })
}

export const saveSearchData = ({ dispatch, data }) => {
  return dispatch({
    type: 'SAVE_SEARCH_DATA',
    payload: { data }
  })
}
