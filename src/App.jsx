import React from 'react'
import { Route, Router, Switch } from './external/router'

import { Store } from './external/store'
import initialState from './store/initialState'
import reducer from './store/reducer'

import Homepage from './containers/Homepage'
import ImdbDetail from './containers/ImdbDetail'
import VolumeDetail from './containers/VolumeDetail'

import Page404 from './components/Page404'

const App = () => {
  return (
    <Store reducer={reducer} initialState={initialState}>
      <Router>
        <Switch>
          <Route exact path='/'>
            <Homepage />
          </Route>
      
          <Route path='/book/:id'>
            <VolumeDetail />
          </Route>
      
          <Route path='/movie/:id'>
            <ImdbDetail />
          </Route>
      
          <Route path='/series/:id'>
            <ImdbDetail />
          </Route>
      
          <Route path='/game/:id'>
            <ImdbDetail />
          </Route>
      
          <Route path='*'>
            <Page404 />
          </Route>
        </Switch>
      </Router>
    </Store>
  )
}

export default App
