import { useContext } from 'react'
import { RouterContext } from './Router'

const useNavigateTo = () => {
  return useContext(RouterContext).navigateTo
}

export default useNavigateTo