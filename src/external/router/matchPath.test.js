import matchPath from './matchPath'

describe('Match Path Function', () => {

  it('should return correct match url and isExact if path are exact', () => {
    const path = '/'
    const toMatchPath = '/'
    const exact = true
    const match = matchPath(toMatchPath, path, exact)
    
    expect(match).not.toBe(null)
    expect(match.url).toBe(path)
    expect(match.isExact).toBe(true)
  })
  
  it('should return null if path are not exact', () => {
    const path = '/something'
    const toMatchPath = '/hello'
    const exact = false
    const match = matchPath(toMatchPath, path, exact)
  
    expect(match).toBe(null)
  })
  
  it('should return null if path doesn\'t match exact but flug exact is passed', () => {
    const path = '/something'
    const toMatchPath = '/something/new'
    const exact = true
    const match = matchPath(path, toMatchPath, exact)
    expect(match).toBe(null)
  })
  
  it('should return correct match object if paths match', () => {
    const toMatchPath = 'something/:id'
    const path = 'something/hello'
    const exact = false
  
    const match = matchPath(path, toMatchPath, exact)
    expect(match).not.toBe(null)
    expect(match.url).toBe(path)
    expect(match.isExact).toBe(false)
  })
  
  
  it('should return correct params array if paths match', () => {
    const toMatchPath = 'something/:id/:name'
    const id = 'testId'
    const name = 'testName'
    const path = `something/${id}/${name}`

    const match = matchPath(path, toMatchPath, false)
    expect(match.params).toStrictEqual({
      id,
      name,
    })
  })
})