import { pathToRegexp } from 'path-to-regexp'

const matchPath = (path, toMatchPath, exact) => {
  const keys = []
  const regexp = pathToRegexp(toMatchPath, keys)
  const match = regexp.exec(path)
  
  if (!match) {
    return null
  }
  
  const isExact = path === toMatchPath
  
  if (exact && !isExact) {
    return null
  }
  
  const [url, ...values] = match
  
  return {
    url,
    isExact,
    params: keys.reduce((acc, key, index) => {
      acc[key.name] = values[index]
      return acc
    }, {}),
  }
}

export default matchPath