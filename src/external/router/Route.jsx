import React, { useContext } from 'react'
import { RouterContext } from './Router'
import PropTypes from 'prop-types'
import matchPath from './matchPath'

const Route = (props) => {
  const { forceMatched, exact, children, path } = props
  const context = useContext(RouterContext)
  
  let match = null
  
  if (path !== '*') {
    match = matchPath(context.currentPath, path, exact)
  }
  
  if (match || forceMatched) {
    return React.cloneElement(children, {
      ...children.props,
      match
    })
  }
  
  return null
}

Route.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
  ]).isRequired,
  path: PropTypes.string.isRequired,
  forceMatched: PropTypes.bool,
  exact: PropTypes.bool
}

Route.defaultProps = {
  forceMatched: false,
  exact: false,
}

export default Route