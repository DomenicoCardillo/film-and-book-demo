import React from 'react'
import PropTypes from 'prop-types'
import useNavigateTo from './useNavigateTo'

const Link = (props) => {
  const { to, children, ...rest } = props
  const navigateTo = useNavigateTo()
  const handleClick = (event) => {
    event.preventDefault()
    navigateTo(to)
  }
  return (
    <a href={to} onClick={handleClick} {...rest}>{children}</a>
  )
}

Link.propTypes = {
  to: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.string
  ]),
}

export default Link