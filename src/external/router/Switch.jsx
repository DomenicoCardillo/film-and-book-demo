import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { RouterContext } from './Router'
import matchPath from './matchPath'

const Switch = (props) => {
  const context = useContext(RouterContext)
  
  let match = null
  let element = null
  
  React.Children.forEach(props.children, (children) => {
    const { path, exact } = children.props
    
    if (match === null && path !== '*') {
      match = matchPath(context.currentPath, path, exact)
      element = children
    }
  })
  
  if (!match) {
    const defaultRoute = props.children.find((children) => children.props.path === '*')
    return React.cloneElement(defaultRoute, { ...defaultRoute, forceMatched: true }) || null
  }
  
  return element
}

Switch.propTypes = {
  children: PropTypes.array.isRequired,
}

export default Switch