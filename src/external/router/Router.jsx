import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'

export const RouterContext = React.createContext()

const Router = (props) => {
  const [currentPath, setCurrentPath] = useState(window.location.pathname)
  
  useEffect(() => {
    window.onpopstate = () => {
      setCurrentPath(window.location.pathname)
    }
  }, [])
  
  const navigateTo = (newPath) => {
    setCurrentPath(newPath)
    window.history.pushState(null, null, newPath)
  }
  
  return (
    <RouterContext.Provider value={{ currentPath, navigateTo }}>
      { props.children }
    </RouterContext.Provider>
  )
}

Router.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]).isRequired,
}

export default Router