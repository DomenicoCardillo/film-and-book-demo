import { useContext } from 'react'
import { StoreContext } from './Store'

const useStore = () => {
  return useContext(StoreContext)
}

export default useStore
