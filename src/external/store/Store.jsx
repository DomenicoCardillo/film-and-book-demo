import React, { createContext, useReducer } from 'react'
import PropTypes from 'prop-types'

export const StoreContext = createContext()

const Store = (props) => {
  const [state, dispatch] = useReducer(props.reducer, props.initialState)
  
  return (
    <StoreContext.Provider value={{ state, dispatch }}>
      {props.children}
    </StoreContext.Provider>
  )
}

Store.propTypes = {
  reducer: PropTypes.func.isRequired,
  initialState: PropTypes.object.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]),
}

export default Store