import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'

// eslint-disable-next-line react/display-name
const InputText = forwardRef((props, ref) => {
  return (
    <input type="text"
      ref={ref}
      value={props.value}
      onChange={(event) => props.onChange(event.target.value)}
      onFocus={props.onFocus}
      onBlur={props.onBlur}
      style={props.customStyle}
      placeholder={props.placeholder}
      className='w-full border-2 border-gray-400 py-1 px-3 rounded focus:outline-none focus:border-primary transition transition-all duration-300' />
  )
})

InputText.propTypes = {
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  customStyle: PropTypes.object,
}

export default InputText