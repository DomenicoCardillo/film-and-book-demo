import React from 'react'
import PropTypes from 'prop-types'

const Layout = (props) => {
  return (
    <div className='container mx-auto px-4'>
      {props.children}
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element.isRequired,
    PropTypes.arrayOf(PropTypes.element)
  ])
}

export default Layout