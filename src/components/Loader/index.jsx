import React from 'react'

const Loader = () => {
  return (
    <div className='Loader'>
      <div className='Loader__item' />
      <div className='Loader__item' />
      <div className='Loader__item' />
    </div>
  )
}

export default Loader