import React, { useEffect, useState, useRef } from 'react'
import PropTypes from 'prop-types'
import InputText from '../InputText'

const SearchInput = (props) => {
  const [canSearch, setCanSearch] = useState(false)
  const valueHasValidLength = props.value.length > 2
  const inputRef = useRef()
  
  useEffect(() => {
    setCanSearch(valueHasValidLength)
    document.addEventListener('keydown', enterKeyCheck)
    return () => document.removeEventListener('keydown', enterKeyCheck)
  }, [canSearch, props.value])
  
  const enterKeyCheck = (event) => {
    if (event.keyCode === 13) {
      handleSearch()
    }
  }
  
  const handleSearch = () => {
    if (canSearch) {
      props.onSearch()
      inputRef.current.blur()
    }
  }
  
  const handleFocus = () => {
    setCanSearch(valueHasValidLength)
  }
  
  const handleBlur = () => {
    setCanSearch(false)
  }
  
  return (
    <div className='relative flex items-center'>
      <InputText
        ref={inputRef}
        value={props.value}
        onChange={props.onChange}
        customStyle={{ paddingRight: 35 }}
        onFocus={handleFocus}
        onBlur={handleBlur}
        placeholder='Search here...'
      />
      <div className='absolute right-0 mr-4'
        role='button' onClick={handleSearch}>
        <div className={`${canSearch ? 'opacity-100' : 'opacity-0'} transition transition-all duration-200`}>
          <svg className="DocSearch-Hit-Select-Icon" width="20" height="20" viewBox="0 0 20 20">
            <g stroke="currentColor" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
              <path d="M18 3v4c0 2-2 4-4 4H2"></path>
              <path d="M8 17l-6-6 6-6"></path>
            </g>
          </svg>
        </div>
      </div>
    </div>
  )
}

SearchInput.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onSearch: PropTypes.func.isRequired,
}

export default SearchInput