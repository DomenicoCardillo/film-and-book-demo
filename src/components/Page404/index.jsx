import React from 'react'
import Layout from '../Layout'
import { Link } from '../../external/router'

const Page404 = () => {
  return (
    <Layout>
      <div className='mt-8 text-center'>
        <h1 className='text-3xl lg:text-5xl mb-3 font-semibold'>404</h1>
        <h2 className='text-xl lg:text-2xl'>This page doesn&apos;t exist, go back to the <Link to='/' className='text-primary'>homepage</Link></h2>
      </div>
    </Layout>
  )
}

export default Page404