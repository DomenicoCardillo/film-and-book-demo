import React from 'react'
import PropTypes from 'prop-types'
import ItemCard from '../ItemCard'

const formatType = (type) => {
  const endWithS = type.substr(-1) === 's'
  return `${type.charAt(0).toUpperCase()}${type.substr(1, type.length)}${!endWithS ? 's' : ''}`
}

const ItemList = (props) => {
  if (props.items.length === 0) {
    return null
  }
  
  return (
    <>
      <h5 className='text-2xl font-semibold my-6 mb-4'>{ formatType(props.type) }</h5>
      <hr className='mb-5 border-gray-400' />
      <div className='flex flex-wrap -mx-3'>
        { props.items.map((item, index) => {
          return (
            <div key={index} className='w-1/2 md:w-1/3 lg:w-1/5 px-3 mb-6'>
              <ItemCard item={item} />
            </div>
          )
        }) }
      </div>
    </>
  )
}

ItemList.propTypes = {
  type: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired
}

export default ItemList