import React from 'react'
import PropTypes from 'prop-types'
import placeholder from '../../assets/images/placeholder.png'
import { useNavigateTo } from '../../external/router'

const ItemCard = (props) => {
  const { item } = props
  const navigateTo = useNavigateTo()
  
  return (
    <div onClick={() => navigateTo(`/${item.type}/${item.id}`)}
      role='button'
      className='ItemCard relative w-full h-full block'>
      <img className='w-full h-full object-cover'
        src={item.thumbnail || placeholder}
        alt={item.title} />
      <div className={`ItemCard__content absolute top-0 left-0 w-full h-full p-4 bg-black bg-opacity-50 text-white ${!item.thumbnail ? 'ItemCard__content--show' : ''}`}>
        <div className='flex flex-col h-full'>
          <span className='text-sm'>{item.year}</span>
          <h6 className='text-sm md:text-base lg:text-xl font-semibold mt-auto'>{item.title}</h6>
        </div>
      </div>
    </div>
  )
}

ItemCard.propTypes = {
  item: PropTypes.object.isRequired
}

export default ItemCard