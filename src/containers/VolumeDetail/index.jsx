import React, { useState, useEffect, useCallback }  from 'react'
import { Link } from '../../external/router'
import Layout from '../../components/Layout'
import Loader from '../../components/Loader'
import { getVolumeById } from '../../lib/books'
import placeholder from '../../assets/images/placeholder.png'

const VolumeDetail = (props) => {
  const [data, setData] = useState({})
  const [error, setError] = useState(null)
  const [pending, setPending] = useState(true)
  
  const handleFetch = useCallback(async () => {
    try {
      const { id } = props.match.params
      const { data, error } = await getVolumeById(id)
      
      if (error) {
        setError(error)
      }
      
      setData(data)
    } catch (error) {
      setError(error.message)
    } finally {
      setPending(false)
    }
  })
  
  useEffect(() => {
    handleFetch()
  }, [])
  
  if (pending) {
    return (
      <div className='text-center mt-20'>
        <Loader />
      </div>
    )
  }
  
  if (error) {
    return (
      <h4 className='text-xl font-semibold text-center mt-20 text-danger'>
        Error: {error}
      </h4>
    )
  }
  
  return (
    <Layout>
      <div className='pt-8 min-h-screen'>
        <div className='mb-12 lg:mb-16'>
          <Link to='/'>
            <i style={{ borderRightColor: 'transparent', borderTopColor: 'transparent' }}
              className='border border-b-2 border-black border-l-2 border-primary h-4 inline-block mr-3 origin-bottom relative rotate-45 transform w-4' />
            <span className='font-semibold text-primary'>Back to search</span>
          </Link>
        </div>
  
        <div className='flex flex-wrap justify-center -mx-3'>
          <div className='lg:w-1/3 px-3'>
            <img src={data.image || placeholder} alt={data.title} className='h-50vh lg:h-auto mb-6 mx-auto' />
          </div>
          <div className='lg:w-2/3 px-3 lg:px-12 lg:text-lg mb-6'>
            <div className='mb-4'>
              <h1 className='text-xl lg:text-3xl font-bold mb-px'>{data.title}</h1>
              <p className='text-gray-600 text-sm lg:text-base'>{data.categories}</p>
            </div>
  
            <h6 className='mb-1'>
              <strong>Release year</strong>: <span>{data.year}</span>
            </h6>
            <h6 className='mb-1'>
              <strong>Language</strong>: <span>{data.language || 'N/D'}</span>
            </h6>
            <h6 className='mb-1'>
              <strong>Authors</strong>: <span>{data.authors}</span>
            </h6>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default VolumeDetail