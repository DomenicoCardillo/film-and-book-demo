import React, { useState, useMemo, useCallback } from 'react'
import { useStore } from '../../external/store'

import Layout from '../../components/Layout'
import SearchInput from '../../components/SearchInput'
import { fetchData } from './functions'
import ItemList from '../../components/ItemList'
import Loader from '../../components/Loader'

import * as actions from '../../store/actions'

const Homepage = () => {
  const { state, dispatch } = useStore()
  const { data, query: defaultQuery } = state
  
  const [centered, setCentered] = useState(data.length === 0)
  const [pending, setPending] = useState(false)
  const [error, setError] = useState(null)
  const [query, setQuery] = useState(defaultQuery)
  
  const handleSearch = useCallback(async () => {
    if (pending) {
      return
    }
    setCentered(false)
    setError(null)
    setPending(true)
    
    actions.saveSearchQuery({ dispatch, query })
    actions.saveSearchData({ dispatch, data: [] })
    
    try {
      const result = await fetchData(query)
      actions.saveSearchData({ dispatch, data: result })
    } catch (error) {
      setError(error.message)
    } finally {
      setPending(false)
    }
  }, [pending, query])
  
  const noData = useMemo(() => data.every(({ items }) => items.length === 0), [data])
  
  const resultElement = useMemo(() => {
    return data.map(({ type, items }, index) => (
      <ItemList key={index} type={type} items={items} />
    ))
  }, [data])
  
  const noDataElement = (
    <div className='text-xl font-semibold text-center mt-20 text-warning'>
      No data found, try with another query!
    </div>
  )
  
  const errorElement = (
    <div className='text-xl font-semibold text-center mt-20 text-danger'>
      Error: {error}
    </div>
  )
  
  const loaderElement = (
    <div className='text-center mt-20'>
      <Loader />
    </div>
  )
  
  return (
    <Layout>
      <div className={`Homepage ${centered ? 'Homepage--centered' : ''}`}>
        <div className='Homepage__header flex items-center flex-wrap'>
          <div className='Homepage__text mt-4'>
            <h1 className='text-xl font-bold'>Search your favourite content</h1>
            <p className='text-sm text-gray-800'>Get information about movies, books and other</p>
          </div>
  
          <div className='Homepage__input-container ml-0 sm:ml-auto'>
            <SearchInput value={query} onChange={setQuery} onSearch={handleSearch} />
          </div>
        </div>
        
        { !pending && error
          ? errorElement
          : null
        }
        
        { pending && data.length === 0
          ? loaderElement
          : null
        }
        
        { !pending && !centered && noData
          ? noDataElement
          : resultElement
        }
      </div>
    </Layout>
  )
}

export default Homepage