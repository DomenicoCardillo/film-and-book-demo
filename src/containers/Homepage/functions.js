import { searchOmdbAllCategoriesByQuery } from '../../lib/omdb'
import { searchVolumesByQuery } from '../../lib/books'

export const fetchData = async (query) => {
  const [omdbResult, volumesResult] = await Promise.all([
    searchOmdbAllCategoriesByQuery(query),
    searchVolumesByQuery(query),
  ])
  
  return [...omdbResult, { type: 'books', items: volumesResult.items }]
}
