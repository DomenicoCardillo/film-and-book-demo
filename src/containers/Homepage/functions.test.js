import { fetchData } from './functions'
import * as omdb from '../../lib/omdb'
import * as books from '../../lib/books'

afterEach(() => {
  jest.clearAllMocks()
})

describe('Homepage functions' , () => {
  it('should return one array of data/category for each omdb category', async () => {
    spyOn(omdb, 'searchOmdbAllCategoriesByQuery').and.returnValue([])
    
    spyOn(books, 'searchVolumesByQuery').and.returnValue({
      items: []
    })
    
    const response = await fetchData('query')
    expect(Array.isArray(response)).toBe(true)
    expect(response[response.length - 1]).toMatchObject({
      type: 'books',
      items: []
    })
  })
})