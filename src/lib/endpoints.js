import config from '../config'

const omdbDbEndpoint = 'https://www.omdbapi.com/'
const omdbImgEndpoint = 'https://img.omdbapi.com/'

export const omdbRequestEndpoint = `${omdbDbEndpoint}?apikey=${config.OMDB_API_KEY}`
export const omdbImageRequestEndpoint = `${omdbImgEndpoint}?apikey=${config.OMDB_API_KEY}`
export const volumesEndpoint = 'https://www.googleapis.com/books/v1/volumes'

const formatParams = (params) => {
  return Object.keys(params)
    .map((key) => `${key}=${encodeURIComponent(params[key].trim())}`)
    .join('&')
}

export const omdbSearchEndpoint = (query, params = {}) => {
  if (!query) {
    throw new Error('Query param not provided')
  }
  
  const getParams = formatParams(params)
  return `${omdbRequestEndpoint}&s=${encodeURIComponent(query.trim())}${getParams ? `&${getParams}` : ''}`
}

export const omdbByIdEndpoint = (id, params = {}) => {
  if (!id) {
    throw new Error('Movie ID not provided')
  }
  
  const getParams = formatParams(params)
  return `${omdbRequestEndpoint}&i=${id}${getParams ? `&${getParams}` : ''}`
}

export const omdbPosterUrl = (id, height = 1200) => {
  if (!id) {
    return null
  }
  
  return `${omdbImageRequestEndpoint}&i=${id}&h=${height}`
}

export const volumesSearchEndpoint = (query, fullTextParams = {}) => {
  if (!query) {
    throw new Error('Query param not provided')
  }
  
  const fullTextQueryParams = Object.keys(fullTextParams)
    .map((key) => `${key}:${encodeURIComponent(fullTextParams[key])}`)
    .join('+')
  
  return `${volumesEndpoint}?q=${encodeURIComponent(query.trim())}${fullTextQueryParams ? `+${fullTextQueryParams}`: ''}`
}

export const volumeByIdEndpoint = (id) => {
  if (!id) {
    throw new Error('Book ID not provided')
  }
  
  return `${volumesEndpoint}/${id}`
}
