import { omdbByIdEndpoint, omdbPosterUrl, omdbSearchEndpoint } from './endpoints'

export const omdbCategories = ['movie', 'series', 'game']

const normalizeBaseMovieData = (movie) => ({
  id: movie.imdbID,
  title: movie.Title,
  year: movie.Year,
  thumbnail: movie.Poster !== 'N/A' ? movie.Poster : null,
  type: movie.Type,
  link: `https://www.imdb.com/title/${movie.imdbID}/`
})

const normalizeMovieData = (movie) => ({
  ...normalizeBaseMovieData(movie),
  director: movie.Director,
  writers: movie.Writer,
  genre: movie.Genre,
  releaseDate: movie.Released,
  plot: movie.Plot
})

export const searchOmdbByQuery = async (query, params) => {
  const url = omdbSearchEndpoint(query, params)
  const response = await fetch(url)
  const responseJson = await response.json()
  
  if (responseJson.Response === 'False') {
    return {
      items: [],
      error: responseJson.Error
    }
  }
  
  return {
    items: responseJson.Search.map(normalizeBaseMovieData),
  }
}

export const searchOmdbAllCategoriesByQuery = async (query) => {
  return await Promise.all(omdbCategories.map(async (type) => {
    const { items } = await searchOmdbByQuery(query, { type })
    return {
      items,
      type
    }
  }))
}

export const getOmdbPoster = async (id, height) => {
  const url = omdbPosterUrl(id, height)
  const { status } = await fetch(url, { method: 'head' })
  return status === 200 ? url : null
}

export const getOmdbById = async (id, params) => {
  const url = omdbByIdEndpoint(id, params)
  const response = await fetch(url)
  const responseJson = await response.json()
  
  if (responseJson.Response === 'False') {
    return {
      data: null,
      error: responseJson.Error
    }
  }
  
  return {
    data: normalizeMovieData(responseJson),
  }
}
