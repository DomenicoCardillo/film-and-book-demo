import {
  omdbRequestEndpoint,
  omdbSearchEndpoint,
  volumesEndpoint,
  volumesSearchEndpoint,
  omdbByIdEndpoint,
  omdbImageRequestEndpoint,
  omdbPosterUrl,
  volumeByIdEndpoint
} from './endpoints'

describe('Endpoint', () => {
  it('should throw if query param is not provided to movies endpoint', () => {
    expect(omdbSearchEndpoint).toThrow(Error)
  })
  
  it('should return movies endpoint with right query params', () => {
    const searchText = 'Harry Potter'
    const url = `${omdbRequestEndpoint}&s=${encodeURIComponent(searchText)}`
    const urlWithParams = `${url}&type=movie`
    const params = {
      type: 'movie',
    }
    
    expect(omdbSearchEndpoint(searchText)).toBe(url)
    expect(omdbSearchEndpoint(searchText, params)).toBe(urlWithParams)
  })
  
  it('should throw if id is not provided to movies endpoint', () => {
    expect(omdbByIdEndpoint).toThrow(Error)
  })
  
  it('should return movie endpoint with right query params', () => {
    const id = 'tt1201607'
    const url = `${omdbRequestEndpoint}&i=${id}`
    const urlWithParams = `${url}&plot=full`
    const params = {
      plot: 'full',
    }
    
    expect(omdbByIdEndpoint(id)).toBe(url)
    expect(omdbByIdEndpoint(id, params)).toBe(urlWithParams)
  })
  
  it('should return null if id isn\'t provided in poster url', () => {
    expect(omdbPosterUrl()).toBe(null)
  })
  
  it('should return image url containing the right id and height', () => {
    const id = 'test'
    const height = 2000
    const correctUrl = `${omdbImageRequestEndpoint}&i=${id}&h=${height}`
    
    expect(omdbPosterUrl(id, height)).toBe(correctUrl)
  })
  
  it('should throw if query param is not provided to volumes endpoint', () => {
    expect(volumesSearchEndpoint).toThrow(Error)
  })
  
  it('should return books endpoint with right query params', () => {
    const searchText = 'Harry Potter'
    const url = `${volumesEndpoint}?q=${encodeURIComponent(searchText)}`
    const urlWithParams = `${url}+inauthor:rowling`
    const params = {
      inauthor: 'rowling'
    }
    
    expect(volumesSearchEndpoint(searchText)).toBe(url)
    expect(volumesSearchEndpoint(searchText, params)).toBe(urlWithParams)
  })
  
  it('should throw if id is not provided to book endpoint', () => {
    expect(volumeByIdEndpoint).toThrow(Error)
  })
  
  it('should return correct book endpoint', () => {
    const id = 'IRoLAQAAQBAJ'
    const url = `${volumesEndpoint}/${id}`
    
    expect(volumeByIdEndpoint(id)).toBe(url)
  })
})