import { volumesSearchEndpoint, volumeByIdEndpoint } from './endpoints'

const publishDateToYear = (date) => {
  if (!date) {
    return 'N/D'
  }
  if (String(date).length === 4) {
    return date
  }
  return new Date(date).getFullYear()
}

const normalizeBaseBookData = ({ id, volumeInfo }) => ({
  id,
  title: volumeInfo.title,
  year: publishDateToYear(volumeInfo.publishedDate),
  thumbnail: volumeInfo.imageLinks?.thumbnail,
  type: 'book',
  link: volumeInfo.canonicalVolumeLink
})

const normalizeBookData = ({ id, volumeInfo }) => ({
  ...normalizeBaseBookData({ id, volumeInfo }),
  authors: volumeInfo.authors,
  categories: volumeInfo.categories,
  language: volumeInfo.language,
  image: volumeInfo.imageLinks?.large
    || volumeInfo.imageLinks?.medium
    || volumeInfo.imageLinks?.thumbnail,
})

export const searchVolumesByQuery = async (query, params) => {
  const url = volumesSearchEndpoint(query, params)
  const response = await fetch(url)
  const responseJson = await response.json()
  
  if (responseJson.totalItems === 0) {
    return {
      items: [],
    }
  }
  
  return {
    items: (responseJson.items ?? []).map(normalizeBaseBookData),
  }
}

export const getVolumeById = async (id) => {
  const url = volumeByIdEndpoint(id)
  const response = await fetch(url)
  const responseJson = await response.json()
  
  if (responseJson.error) {
    return {
      data: null,
      error: responseJson.error.message
    }
  }
  
  return {
    data: normalizeBookData(responseJson)
  }
}
