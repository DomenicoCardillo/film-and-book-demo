import { getOmdbById, omdbCategories, searchOmdbByQuery, searchOmdbAllCategoriesByQuery, getOmdbPoster } from './omdb'
import { mockFetch } from '../test/mock-fetch'
import { omdbPosterUrl } from './endpoints'

mockFetch()

beforeEach(() => {
  fetch.mockClear()
})

describe('Movie API', () => {
  it('should return array if there are movies', async () => {
    mockFetch({
      Response: 'True',
      Search: new Array(1).fill({}),
    })
    
    const { items } = await searchOmdbByQuery('query')
    expect(items).toHaveLength(1)
    expect(Object.keys(items[0]).length).toBeGreaterThan(0)
  })
  
  it('should return empty array and error message if there are no movies', async () => {
    const errorMessage = 'No movies'
    
    mockFetch({
      Response: 'False',
      Error: errorMessage
    })
    
    const { items, error } = await searchOmdbByQuery('query')
    expect(items).toHaveLength(0)
    expect(error).toBe(errorMessage)
  })
  
  it('should return one array of items/category for each omdb category', async () => {
    mockFetch({
      Response: 'True',
      Search: [],
    })
    
    const response = await searchOmdbAllCategoriesByQuery('query')
    expect(response).toHaveLength(omdbCategories.length)
    expect(response[0]).toEqual(expect.objectContaining({
      items: [],
      type: omdbCategories[0]
    }))
  })
  
  it('should return object data if movie is found', async () => {
    mockFetch({
      Response: 'True',
    })
    
    const { data } = await getOmdbById('valid_id')
    expect((Object.keys(data)).length).toBeGreaterThan(0)
  })
  
  it('should return null data and error message if movie is not found', async () => {
    const errorMessage = 'Invalid Id provided'
    
    mockFetch({
      Response: 'False',
      Error: errorMessage
    })
    
    const { data, error } = await getOmdbById('invalid_id')
    expect(data).toBe(null)
    expect(error).toBe(errorMessage)
  })
  
  describe('Get poster url', () => {
    it('should return null if poster isn\'t found', async () => {
      mockFetch(null, 404)
      const response = await getOmdbPoster('test_id')
      expect(response).toBe(null)
    })
  
    it('should return correct url if poster is found', async () => {
      const id = 'test_id'
      const correctUrl = omdbPosterUrl(id)
      
      mockFetch(null, 200)
      const response = await getOmdbPoster(id)
      
      expect(response).toBe(correctUrl)
    })
  })
})