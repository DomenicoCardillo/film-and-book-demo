import { getVolumeById, searchVolumesByQuery } from './books'
import { mockFetch } from '../test/mock-fetch'

mockFetch()

beforeEach(() => {
  fetch.mockClear()
})

describe('Books API', () => {
  it('should return array if there are books', async () => {
    mockFetch({
      items: new Array(1).fill({
        volumeInfo: {}
      }),
      totalItems: 1
    })
    
    const { items } = await searchVolumesByQuery('query')
    expect(items).toHaveLength(1)
    
    const [first] = items
    expect(Object.keys(first).length).toBeGreaterThan(0)
  })
  
  it('should return empty array and error message if there are no books', async () => {
    mockFetch({
      totalItems: 0
    })

    const { items } = await searchVolumesByQuery('query')
    expect(items).toHaveLength(0)
  })
  
  it('should return object data if book is found', async () => {
    const id = 'test_id'
    
    mockFetch({
      id,
      volumeInfo: {}
    })
    
    const { data } = await getVolumeById('test_id')
    expect((Object.keys(data)).length).toBeGreaterThan(0)
    expect(data.id).toBe(id)
  })
  
  it('should return null data and error message if book is not found', async () => {
    const message = 'Invalid Id provided'
    
    mockFetch({
      error: { message }
    })
    
    const { data, error } = await getVolumeById('test_id')
    expect(data).toBe(null)
    expect(error).toBe(message)
  })
})